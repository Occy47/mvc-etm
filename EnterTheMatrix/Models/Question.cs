﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterTheMatrix.Models
{
    public class Question
    {
        public int QuestionId { get; set; }
        public string Riddle { get; set; }
        public string Answer { get; set; }
        public string PossibleAnswerOne { get; set; }
        public string PossibleAnswerTwo { get; set; }
        public string PossibleAnswerThree { get; set; }
    }
}
