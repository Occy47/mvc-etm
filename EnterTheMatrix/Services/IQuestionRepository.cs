﻿using EnterTheMatrix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterTheMatrix.Services
{
    public interface IQuestionRepository
    {
        IQueryable<Question> Questions { get; }
    }
}
