﻿using EnterTheMatrix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterTheMatrix.Services
{
    public class QuestionRepository : IQuestionRepository
    {
        public IQueryable<Question> Questions => new List < Question > {
            new Question { Riddle="What truth? -That you are a slave, Neo. Like everyone else you were born into bondage. Into a prison that you cannot taste or see or touch. A prison for your mind.",
                Answer ="Morpheus", PossibleAnswerOne="Trinity", PossibleAnswerTwo="Morpheus", PossibleAnswerThree="Agent Smith"},
            new Question { Riddle="I'm trying to free your mind, Neo. But I can only show you the door. You're the one that has to walk through it.",
                Answer ="Morpheus", PossibleAnswerOne="Trinity", PossibleAnswerTwo="Oracle", PossibleAnswerThree="Morpheus"}

        }.AsQueryable<Question>();

    }
}
